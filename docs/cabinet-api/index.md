# API доступа к информации о карте

**История изменений:**

* 27 февраля 2017 года (версия схемы 1.3.70227):
  * Изменены (приведены к единому виду) поля, описывающие ссылку на транзакцию в методах `/api/v1/card/history`, `/api/v1/ticket`, `/api/v1/ticket/info`, `/api/v1/ticket/create`, а именно:
      * `transaction-id` содержит код транзакции (UUID)
      * `transaction-date` содержит "учётную" дату транзакции (дата начала транзакции по UTC, без времени)
      * `transaction-row-num` содержит номер строки чека (целое число, нумерация с 1)
* 30 января 2017 года (версия схемы 1.2.70130):
  * В ответ метода `/api/v1/card/history` добавлено поле `row-num` (номер строки чека);
  * Добавлен метод `/api/v1/ticket/drugstores` (получение списка аптек);
  * Добавлен метод `/api/v1/ticket/create` (создание новой претензии); 
* 9 февраля 2016 (версия схемы 1.1.60209):
  * Добавлен метод `/api/v1/profile`;
* 12 января 2016 года:
  * Начальная версия документа.

## Общие сведения

Получить информацию о карте (история операций, остатки лимитов и т.п.), а также активировать новую карту, можно с помощью REST API личного кабинета.

Объем функций API не превышает объем функций самого личного кабинета, таким образом добавлять/удалять карты, менять тарифный план, регистрировать/отменять продажу и т.п. - с помощью API **нельзя**.

Также на данный момент (временно) через API нельзя создать (оставить) новую претензию (можно только добавить сообщение к уже имеющейся), при этом в пользовательском интерфейсе кабинета такой функционал присутствует. Расширение API соответствующими методами выполнено сделано позднее.

## Контроль доступа

Для многих методов нужна авторизации клиента, для чего используются два механизма:

* общий preshared key, дающий доступ ко всей информации о всех картах - для запросов от "доверенных серверов"
* OAuth-токен, дающий доступ только к определенной информации конкретной карты (предварительно "одобренной" держателем при авторизации приложения) - для пользовательских приложений (вроде клиентов мобильных платформ).

### Авторизация через preshared key

Ключ (представляет собой строку символов) должен передаваться с каждым запросом в HTTP-заголовке **X-OLEKSTRA-API-KEY**, например:

    Host: cabinet.olekstra.ru
    Content-Length: 128
    X-OLEKSTRA-API-KEY: abcHkdfjU9_6hs=ksd
    Content-Type: application/xml

    <?xml version="1.0" encoding="UTF-8"?>
    <card-details-request number="1390377461238" xmlns="http://www.olekstra.ru/api/2015/1"/>

При отсутствии заголовка или неправильном значении ключа вернется ответ с кодом 401.

### Авторизация через OAuth-токен

Токен должен быть предварительно получен с использованием стандартного протокола OAuth2.

Для большей безопасности используются scope (области):

* **card_info** - общая информация о карте, текущие остатки лимитов
* **card_log** - история операций по карте
* **ticket** - информация о претензиях (чтение, добавление сообщений)

Для успешного выполнения метода используемый oauth-токен должен включать соответствующий scope (необходимо было запросить его при запросе токена).

Конечные точки для работы с токенами:

* AuthorizationEndpoint: **/oauth/authorize**
* TokenEndpoint: **/oauth/token**

**Важно! Не все методы доступны с использованием OAuth-токена, в частности недоступны методы, где не фигурирует номер карты в запросе и таким образом заранее неизвестно к какой карте запрос относится.** Такие методы доступны только с использованием авторизации через preshared key. В документации к методу это указывается явно.

## XSD-схема взаимодействия

Все вызовы делаются методом POST, запрос и ответ содержат данные в формате XML согласно XSD схеме api-v1.xsd.

## Перечень методов API

### Информация о текущем пользователе/карте (`/api/v1/profile`)

* Необходимый scope: нет (любой)
* **Необходима авторизация по OAuth**, так как токен содержит информацию о "текущем" пользователе
* Запрос: пустой, без BODY (можно использовать метод GET)
* Ответ:
  ![profile-response](profile-response.png "profile-response")

### Информация о карте (`/api/v1/card`)

* Необходимый scope: `card_info`
* Запрос:
  ![card-details-request](card-details-request.png "card-details-request")
* Ответ:
  ![card-details-response](card-details-response.png "card-details-response")


### Информация об остатке лимитов на карте (`/api/v1/card/limit`)

* Необходимый scope: `card_info`
* Запрос:
  ![card-limits-request](card-limits-request.png "card-limits-request")
* Ответ:
  ![card-limits-response](card-limits-response.png "card-limits-response")


### История операций по карте (`/api/v1/card/history`)

* Необходимый scope: `card_log`
* Запрос:
  ![card-history-request](card-history-request.png "card-history-request")
* Ответ:
  ![card-history-response](card-history-response.png "card-history-response")


### Список претензий за период (`/api/v1/ticket`)

* Необходимый scope: `ticket`
* Запрос:
  ![card-tickets-request](card-tickets-request.png "card-tickets-request")
* Ответ:
  ![card-tickets-response](card-tickets-response.png "card-tickets-response")


### Информация об отдельной претензии (`/api/v1/ticket/info`)

* Необходимый scope: `ticket`
* Запрос:
  ![ticket-info-request](ticket-info-request.png "ticket-info-request")
* Ответ:
  ![ticket-info-response](ticket-info-response.png "ticket-info-response")


### Добавление сообщения/комментария к претензии (`/api/v1/ticket/post`)

* Необходимый scope: `ticket`
* Запрос:
  ![ticket-post-request](ticket-post-request.png "ticket-post-request")
* Ответ: без содержимого, просто HTTP-статус 200


### Метаданные о претензиях (`/api/v1/ticket/meta`)

* Необходимый scope: `ticket`
* Запрос: пустой, без BODY (можно использовать метод GET)
* Ответ:
  ![tickets-meta-info-response](tickets-meta-info-response.png "tickets-meta-info-response")


### Создание новой претензии (`/api/v1/ticket/create`)

* Необходимый scope: `ticket`
* Запрос:
  ![ticket-create-request](ticket-create-request.png "ticket-create-request")
* Ответ:
  ![ticket-create-response](ticket-create-response.png "ticket-create-response") 


### Список аптек (`/api/v1/ticket/drugstores`)

* Необходимый scope: `ticket`
* Запрос: пустой, без BODY (можно использовать метод GET)
* Ответ:
  ![drugstore-list-response](drugstore-list-response.png "drugstore-list-response")


### Активация карты, шаг 1 (`/api/v1/card/activate/step/1`)

* Необходимый scope: нет (доступно без авторизации)
* Запрос:
  ![card-activate-step1-request](card-activate-step1-request.png "card-activate-step1-request")
* Ответ:
  ![card-activate-step1-response](card-activate-step1.png "card-activate-step1-response")

При успешном выполнении метода на указанный номер телефона будет отправлен проверочный код. Необходимо запросить у пользователя этот код и после использовать его на втором шаге.


### Активация карты, шаг 2 (`/api/v1/card/activate/step/2`)

* Необходимый scope: нет (доступно без авторизации)
* Запрос:
  ![card-activate-step2-request](card-activate-step2-request.png "card-activate-step2-request")
* Ответ:
  ![card-activate-step2-response](card-activate-step2.png "card-activate-step2-response")


### Перечень карт, закрепленных за номером телефона (`/api/v1/phone`)

* Необходимый scope: нет, недоступно через OAuth
* **Необходима авторизация по preshared key**
* Запрос:
  ![phone-cards-request](phone-cards-request.png "phone-cards-request")
* Ответ:
  ![phone-cards-response](phone-cards-response.png "phone-cards-response")
