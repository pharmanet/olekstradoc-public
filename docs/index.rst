﻿Олекстра
========

Данная документация предназначена для партнеров Олекстры.

Внимание! В декабре 2016 года документация была реструктурирована для облегчения чтения и улучшения понимания. Старый вариант документации доступен `по адресу /ru/v1 </ru/v1>`_.

.. toctree::
   :maxdepth: 2
   :caption: Авторизация чеков

   authcard/principles
   authcard/api
   authcard/data-exchange

* :download:`XSD-схема взаимодействия с сервером (api.xsd) <authcard/api.xsd>`
* :download:`XSD-схема файла прайс-листа (assortim.xsd) <authcard/assortim.xsd>`


.. toctree::
   :maxdepth: 2
   :caption: API личного кабинета

   cabinet-api/index

* :download:`XSD-схема взаимодействия с сервером (api-v1.xsd) <cabinet-api/api-v1.xsd>` (версия 1.3.70227)


.. toctree::
   :maxdepth: 2
   :caption: Трекинг заказов

   exchange/index

* :download:`XSD-схема по заказам (orders.xsd) <exchange/orders.xsd>` (версия 1.0.70217)


.. toctree::
   :maxdepth: 2
   :caption: Шина обмена сообщениями

   messaging/index

* :download:`XSD-схема базовых объектов (objects.xsd) <messaging/objects.xsd>` (версия 1.13.60330)
* :download:`XSD-схема сообщений (messages.xsd) <messaging/messages.xsd>` (версия 1.18.70912)
* :download:`XSD-схема zabota (zabota.xsd) <messaging/zabota.xsd>` (версия 1.0.60418)


.. toctree::
   :maxdepth: 2
   :caption: Заказы на сайтах-партнерах

   oksana/index

* :download:`XSD-схема по заказам (oksana.xsd) <oksana/oksana.xsd>` (версия 1.3.200724)
